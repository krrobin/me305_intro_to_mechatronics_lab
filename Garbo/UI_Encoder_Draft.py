##  @file UI_Encoder.py
#   Documentation for UI_Encoder.py, which contains the code for the User Interface to work properly.
#   This code will allow the user to choose from three different actions that include zeroing the encoder position, printing out the encoder position, and printing out the encoder delta.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/UI_Encoder.py
#
#   @author Keanau Robin
#
#

# import time
import utime
import Encoder_Class
from pyb import UART
my_uart = UART(2)

class UserInterface:
    '''
    @brief      This class includes the finite state machine for our User Interface.
    @details    This class includes the finite state machine that processes the user input of p, z, or d to give a specific output for each specified input.
    '''
    
    S0_INIT               = 0
    
    S1_INPUTS_PROCESSING  = 1
    
    def __init__(self,interval):
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        # self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        # self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_INPUTS_PROCESSING)
            
            elif(self.state == self.S1_INPUTS_PROCESSING):
                # Run State 1 Code
                if(my_uart.any()):
                    if my_uart.readchar().decode(122): # 122 is ASCII for z
                        Encoder_Class.set_position(0)
                        print(Encoder_Class.set_position())
                        print('User must input "z" to zero the encoder location, "p" to print out the encoder position, or "d" to print out the encoder delta')
                    elif my_uart.readchar().decode(112): # 122 is ASCII for p
                        Encoder_Class.get_position()
                        print(Encoder_Class.get_position())
                        print('User must input "z" to zero the encoder location, "p" to print out the encoder position, or "d" to print out the encoder delta')
                    elif my_uart.readchar().decode(100): # 122 is ASCII for z
                        Encoder_Class.get_delta()
                        print(Encoder_Class.get_delta())
                        print('User must input "z" to zero the encoder location, "p" to print out the encoder position, or "d" to print out the encoder delta')
                    else:
                        print('User must input "z" to zero the encoder location, "p" to print out the encoder position, or "d" to print out the encoder delta')
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
             # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            # self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState            
        
    
    
    
