##  @file Encoder_main.py
#   Documentation for Encoder_main.py, which is the code that runs the Encoder and UI tasks.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/Encoder_main.py
#
#   @author Keanau Robin
#
#

from FSM_Encoder import TaskEncoder
from UI_Encoder import UserInterface
from Encoder_Class import Encoder

myencoder = Encoder()


# Creating the task objects
task1 = TaskEncoder(.1, myencoder)
task2 = UserInterface(.1)

# Run the tasks in sequence over and over again
while True: # effectively while(True):
    task1.run()
    task2.run()

