##  @file UI_Encoder.py
#   Documentation for UI_Encoder.py, which contains the code for the User Interface to work properly.
#   This code will allow the user to choose from three different actions that include zeroing the encoder position, printing out the encoder position, and printing out the encoder delta.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/UI_Encoder.py
#
#   @author Keanau Robin
#
#
import shares
import utime
from FSM_Encoder import TaskEncoder
from pyb import UART

class UserInterface:
    '''
    @breif      This class includes the finite state machine for our User Interface.
    @details    This class includes the finite state machine that processes the user input of p, z, or d to give a specific output for each specified input.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, interval):
        '''
        @param interval An integer number of microseconds between desired runs of the task
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        

    def run(self):
        '''
        @brief     Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
                print('User must input "z" to zero the encoder location, "p" to print out the encoder position, or "d" to print out the encoder delta')
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                # Run State 1 Code
                
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    shares.cmd = self.ser.readchar()
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                # Run State 2 Code
                if shares.resp:
                    
                    # self.ser.writechar(shares.resp)
                    shares.resp = None
                    print('User must input "z" to zero the encoder location, "p" to print out the encoder position, or "d" to print out the encoder delta')
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief     Updates the variable defining the next state to run
        '''
        self.state = newState

