##  @file FSM_Encoder.py
#   Documentation for FSM_Encoder.py, which contains code that runs the update() method through multiple iterations. The update() method updates the user on the position of the encoder.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/FSM_Encoder.py
#
#   @author Keanau Robin
#
#

from Encoder_Class import Encoder
import utime
import shares
# import time

class TaskEncoder:
    '''
    @brief     A finite state machine that updates the user on the encoder location/ticks at a regular interval.
    @details   This class implements a finite state machine that updates the user on the encoder location at a regular interval
    '''
    
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT             = 0
    
    S1_UPDATE_POSITION  = 1
    
    def __init__(self,interval,Encoder):
        
        self.Encoder = Encoder
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        # self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATE_POSITION)
            
            elif(self.state == self.S1_UPDATE_POSITION):
                # Run State 1 Code
                
                self.Encoder.update()

                if shares.cmd:
                
                    
                    if shares.cmd == 122:
                        self.Encoder.set_position(0)
                        shares.resp = self.Encoder.position
                        print(shares.resp)
                    elif shares.cmd == 112:
                        self.Encoder.get_position()
                        shares.resp = self.Encoder.position
                        print(shares.resp)
                    elif shares.cmd == 100:
                        self.Encoder.get_delta()
                        shares.resp = self.Encoder.delta
                        print(shares.resp)
                    shares.cmd = None
                
                else:
                    # Invalid state code (error handling)
                    pass
                        
                self.runs += 1
            
                 # Specifying the next time the task will run
                self.next_time = utime.ticks_add(self.next_time, self.interval)
                # self.next_time = self.next_time + self.interval

    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState            
        