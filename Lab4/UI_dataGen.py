##  @file UI_dataGen.py
#   Documentation for UI_dataGen.py, which contains the FSM that is in charge of collecting data from the encoder. The data collection is done through the use of multiple functions
#   from the Encoder_Class.py file. With these functions implemented into the FSM, the data collection is made possible.
#   The data collection does not start until the user types G, which is processed by the UI_Frontend.py file, and that file uses that user input to prompt the UI_dataGen.py file to
#   begin collecting data. If the user types S, the front end code will use that input and prompt the UI_dataGen.py file to stop data collection.   
#   The encoder position is being collected along with a timestamp for each value. The data collection is being done at 5 Hz for 10 seconds.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab4/UI_dataGen.py
#
#   @author Keanau Robin
#
#

from Encoder_Class import Encoder
import utime
from pyb import UART
myuart = UART(2)
ser = myuart
# import time

class DataGen:
    '''
    @brief     A finite state machine that collects the data of the encoder's position.
    @details   This class implements a finite state machine that goes through a data collection process in which the the encoder position is collected in degrees. With each value,
               there is a timestamp that corresponds. At the end of data collection, this class sends the array of data to the front end to begin the plotting process.
    '''
    
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT             = 0
    
    S1_WAIT_FOR_CMD     = 1
    
    S2_COLLECT_DATA     = 2
    
    def __init__(self,interval,Encoder):
        
        self.Encoder = Encoder
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        # self.next_time = self.start_time + self.interval
        
        self.ser = UART(2)
        
        self.time = []
        self.pos = []
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S1_WAIT_FOR_CMD):
                # Run State 1 Code            
                if myuart.any() != 0:
                    if self.ser.readchar() == 71:
                        self.transitionTo(self.S2_COLLECT_DATA)
                    else:
                        pass
                        # print('Please type G to begin data collection.\nOnce data collection starts, press S if you wish to stop data collection.')
                else:
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
            
            elif(self.state == self.S2_COLLECT_DATA):
                # Run State 2 Code
                
                self.Encoder.update()
                # self.Encoder.get_position()
                self.time.append(round(self.interval*self.runs*1e-3, 1))    # made sure to convert the timestamp from milliseconds to seconds
                self.pos.append(round(self.Encoder.position*360/700, 2))    # made sure to convert the encoder position to degrees

                if self.interval*self.runs >= 10000:
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    ser.write('{:},{:}'.format(self.time, self.pos))
                    
                    self.time.clear()
                    self.pos.clear()
                    self.Encoder.set_position(0)
                    self.runs = -1
                    # print('\nData collection complete. \nPlease type G to begin new data collection.\nIf you wish to stop any data collection, please type S.')
                elif myuart.any() != 0:
                    if self.ser.readchar() == 83:
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                        ser.write('{:},{:}'.format(self.time,self.pos))
                        self.time.clear()
                        self.pos.clear()
                        self.Encoder.set_position(0)
                        self.runs = -1
                        # print('\nData collection has been stopped.\nPlease type G to begin new data collection.')
                    else:
                        pass
                    
                    
                else:
                    pass
                
                self.runs += 1
                    
            else:
                    # Invalid state code (error handling)
                pass
            
                # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                # self.next_time = self.next_time + self.interval

    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState            
        
