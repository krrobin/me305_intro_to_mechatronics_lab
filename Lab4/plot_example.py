# Plot example given to us in lab for Lab4

import matplotlib.pyplot as plt
import numpy as np

time = np.array([0,1,2,3,4,5,6,7,8,9,10])
vals = time**2  # squares the time array

plt.plot(time,vals,'ko-')
plt.xlabel('Time [s]')
plt.ylabel('Degrees [rad]')

plt.show() # need this, so it would actually show your plot

# examp = np.array([0,1,2,3,4,5,6])
# np.savetxt('examp.csv', examp, delimiter =',') # delimiter is what is used to separate the values in your array
#then, run these two lines of code and type "ls" in console
