##  @file main.py
#   Documentation for main.py, which is the code that runs the UI_dataGen.py from the Nucleo. In this main file, the frequeny of the data collection is declared. For this lab, we wanted
#   a frequency of 5 Hz for 10 seconds. So, the interval value was set to 200ms or 0.2s.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab4/main.py
#
#   @author Keanau Robin
#
#

# from UI_Frontend import 
from UI_dataGen import DataGen
from Encoder_Class import Encoder

myencoder = Encoder()

# Creating the task objects
task1 = DataGen(.2, myencoder)


# Run the tasks in sequence over and over again
while True: # effectively while(True):
    task1.run()

