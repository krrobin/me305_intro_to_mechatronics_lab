##  @file Lab7Backend.py
#   Documentation for Lab7Backend.py, which contains an FSM that is in charge of receiving the desired Kp from the Lab7Frontend.py file. Once the Kp is read, the backend createsa reference velocity
#   array identical to the reference velocity plotted in the Lab7Frontend.py. This reference velocity array is sent bit-by-bt to the Controller_TaskLab7.py file along with the Kp value. This file also
#   receives an array of the actual motor velocity, motor position, and the timestamp corresponding to each data point. This data is appended and sent to the Lab7Frontend.py. 
#
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab7/Lab7Backend.py
#
#   @author Keanau Robin
#
#

import shares
from pyb import UART
from array import array
# ser=UART(2)
# ser = pyb.USB_VCP()
import utime


class Lab7_Backend:
    '''
    @brief     A finite state machine that sends the user's desired Kp from the frontend to the Controller_TaskLab7.py, and sends the motor's actual velocity from the Controller Task to the Frontend.
    @details   This class implements a finite state machine that sends the user's desired Kp from the Frontend to the Controller Task, along with a reference velocity. Then, this class takes
               the motor velocity data from the Controller Task and sends it to the Frontend for plotting. 
    '''
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT                       = 0
    
    S1_RECEIVE_USER_INPUTS        = 1
    
    S2_SEND_TO_TASK               = 2
    
    S3_RECEIVE_FROM_TASK          = 3
    
    S4_SEND_TO_FRONT              = 4
    
    
    
    def __init__(self,interval):
        
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        # self.next_time = self.start_time + self.interval

        self.Omega_Actual = []
        
        self.Time_Actual = []
        
        self.Position_Actual = []
        
        self.ser = UART(2)
        
        self.ref_velocity = array('f',[])
        
        self.ref = open('UpdatedRefBackend.csv')
        
        
        while True:
            
            self.line = self.ref.readline()
            
            if self.line == '':
                break
        
            else:
                v = self.line.strip()
                self.ref_velocity.append(float(v))

        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
            
            elif(self.state == self.S1_RECEIVE_USER_INPUTS):
                # Run State 1 Code
                if self.ser.any() != 0:
                    Front_Inputs = self.ser.readline().decode('ascii')
                    shares.Kp = float(Front_Inputs)
                    self.transitionTo(self.S2_SEND_TO_TASK)
                else:
                    self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
                    self.runs = 0

                    
            elif(self.state == self.S2_SEND_TO_TASK):
                # Run State 2 Code
                shares.Om_ref = self.ref_velocity[self.runs]
                self.transitionTo(self.S3_RECEIVE_FROM_TASK)
                
                self.runs += 1
                
            elif(self.state == self.S3_RECEIVE_FROM_TASK):
                if shares.Om_act != None and shares.Time_act != None:
                    self.Omega_Actual.append(shares.Om_act)
                    self.Time_Actual.append(shares.Time_act)
                    self.Position_Actual.append(shares.Pos)
                    shares.Om_act = None
                    shares.Time_act = None
                    shares.Pos = None
                    self.transitionTo(self.S2_SEND_TO_TASK)
                else:
                    pass
                if self.runs >= 281:
                    self.transitionTo(self.S4_SEND_TO_FRONT)
                    
            elif(self.state == self.S4_SEND_TO_FRONT):
                    
                    self.ser.write('{:};{:};{:}\r\n'.format(str(self.Omega_Actual), str(self.Time_Actual), str(self.Position_Actual)).encode('ascii'))
                    self.runs = 0
                    self.Omega_Actual = []
                    self.Time_Actual = []
                    self.Position_Actual = []
                    shares.Kp = None
                    self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
                    
            else:
                    # Invalid state code (error handling)
                pass
            
                # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                # self.next_time = self.next_time + self.interval


    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState            
        