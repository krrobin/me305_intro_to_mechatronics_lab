##  @file Lab7Frontend.py
#   Documentation for Lab7Frontend.py, which contains two functions: sendUserInputs and receiveBackendArrays. This frontend code is what the user is interfacing with in order to 
#   successfully input a Kp controller gain value into the system. This frontend file will send the desired Kp value to the Lab7backend.py file. This file also receives arrays that
#   represent actual time [s] and actual motor velocity [rpm], and then plots this data on a plot that also includes the step input line. 
#
#   UPDATES: This Frontend differs from the Lab 6 Frontend because we are now given a specific reference profile to track as opposed to a simple step input used in Lab 6. The plot 
#   will include two subplots: one for the reference velocity plotted against the actual velocity in rpm, and the other subplot is for the reference position plotted against the actual 
#   position in degrees In State 3, there is an added section that calculates the performance metric, J, for every current plot being plotted. There is also a new State 4 that basically 
#   serves as a stopping point and makes sure the front end doesn't plot or calculate any further. 
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab7/Lab7Frontend.py
#
#   @author Keanau Robin
#


import serial
import matplotlib.pyplot as plt
import time

ser = serial.Serial(port='COM5',baudrate=115273,timeout=1) # same as myuart = UART(2) but in Serial because we are dealing with serial communication in the laptop, not Nucleo



class Lab7_Frontend:
    '''
    @brief     A finite state machine that sends a Kp to the backend, and then receives motor velocity data to plot against time.
    @details   This class implement a finite state machine that the user runs to get a plot of the DC motor's velocity for a Kp value chosen by the user. Once the user puts their
               desired Kp value on the console, that is sent to the backend which will begin the data collection. This class will receive motor velocity data and will plot that data.
    '''   
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT                       = 0
    
    S1_SEND_TO_BACKEND            = 1
    
    S2_RECEIVE_FROM_BACKEND       = 2
    
    S3_PLOT_AND_CALC_J            = 3
    
    S4_STOP                       = 4
    
    
    def __init__(self,interval):
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        # self.interval = int(interval*1e3)
        self.interval = interval
        
        ## The timestamp for the first iteration
        # self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        # self.next_time = utime.ticks_add(self.start_time, self.interval)
        self.next_time = self.start_time + self.interval
        
        self.J = 0
        
        self.summation = 0
        
        self.Kp = 0
        
        self.TimeAct = []
        
        self.OmegaAct = []
        
        self.PosAct = []
        
        self.step_input = []
        
        self.ref_time = []
        self.ref_velocity = []
        self.ref_position = []
        self.ref = open('UpdatedRef.csv')
        
        while True:
            self.line = self.ref.readline()
            
            if self.line == '':
                break
        
            else:
                (t,v,x) = self.line.strip().split(',')
                self.ref_time.append(float(t))
                self.ref_velocity.append(float(v))
                self.ref_position.append(float(x))
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        # self.curr_time = utime.ticks_ms()
        self.curr_time = time.time()
        # if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_SEND_TO_BACKEND)
            
            elif(self.state == self.S1_SEND_TO_BACKEND):
                # Run State 1 Code
                self.sendUserInputs()
                if self.Kp != 0:
                    self.transitionTo(self.S2_RECEIVE_FROM_BACKEND)
                    
                else:
                    self.transitionTo(self.S1_SEND_TO_BACKEND)
                
            elif(self.state == self.S2_RECEIVE_FROM_BACKEND):
                # Run State 2 Code
                if self.runs >= 17:
                    self.BackendArrays = ser.readline().decode('ascii')
                    if self.BackendArrays != None:
                        self.receiveBackendArrays()
                        self.transitionTo(self.S3_PLOT_AND_CALC_J)
                else:
                    self.runs += 1
                    self.transitionTo(self.S2_RECEIVE_FROM_BACKEND)
                    
            elif(self.state == self.S3_PLOT_AND_CALC_J):
                
                plt.subplot(2,1,1)
                plt.plot(self.ref_time[1:280], self.ref_velocity[1:280])
                plt.plot(self.TimeAct[1:280], self.OmegaAct[1:280])
                plt.ylabel('Shaft Velocity [rpm]')
                plt.legend(['Reference Velocity', 'Actual Velocity'])
                # plt.title('For a Kp=0.4')
                # plt.title('For a Kp=0.5')
                plt.title('For a Kp=0.6')
                
                plt.subplot(2,1,2)
                plt.plot(self.ref_time[1:280], self.ref_position[1:280])
                plt.plot(self.TimeAct[1:280], self.PosAct[1:280])
                plt.xlabel('Time [s]')
                plt.ylabel('Position [deg]')
                plt.legend(['Reference Position', 'Actual Position'], loc = 2)
                
                for n in range (1, 270):
                    self.summation += ((self.ref_velocity[n] - self.OmegaAct[n])**2 + (self.ref_position[n] - self.PosAct[n])**2)
                self.J = (1/280) * self.summation
                
                print('The Performance Metric, J, is:', self.J)

                self.OmegaAct = []
                self.TimeAct = []
                self.PosAct = []
                self.ref_time = []
                self.ref_velocity = []
                self.ref_position = []
                self.summation = 0
                self.J = 0
                self.transitionTo(self.S4_STOP)
            
            elif(self.state == self.S4_STOP):
                if self.runs >= 17:
                    self.transitionTo(self.S4_STOP)
                    print('Data collection is complete. Plot will appear after stopping current command. Please stop the current command and rerun the file for a new plot and J value.')
                    self.runs = 0
                    ser.close()
                else:
                    pass
                if self.runs >= 0:
                    self.transitionTo(self.S4_STOP)
                
            else:
                    # Invalid state code (error handling)
                pass
            
                # Specifying the next time the task will run
            # self.next_time = utime.ticks_add(self.next_time, self.interval)
            self.next_time = self.next_time + self.interval


    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    
    def sendUserInputs(self):
        '''
        @brief      This function prompts the user to input a desired Kp value for the closed loop system.
        @details    This function prompts the user to input a desired Kp value, which will be sent to the backend to begin data collection. Decimal values are allowed.
        '''
        self.Kp = float(input('Please input your desired Kp value:'))
        print('Kp was inputted')
        ser.write('{:f}\r\n'.format(self.Kp).encode('ascii'))
    
    
    def receiveBackendArrays(self):
        '''
        @brief      This function reads the data received from the backend, and then plots that data.
        @details    This function reads the data received from the backend, and then plots te data for the user to observe the motor velocity for a given Kp value. 
        '''
        
        self.Backend_List = self.BackendArrays.strip('[]\r\n').split('];[')
        self.OmegaActual = (self.Backend_List[0].split(','))
        print(self.OmegaActual)
        self.TimeActual = (self.Backend_List[1].split(','))
        print(self.TimeActual)
        self.PositionActual = (self.Backend_List[2].split(','))
        for n in range (len(self.OmegaActual)):
            self.OmegaAct.append(float(self.OmegaActual[n]))
            self.TimeAct.append(float(self.TimeActual[n])/1000)
            self.PosAct.append(float(self.PositionActual[n]))
            self.Backend_List = None
            
if __name__ == '__main__':
    
    FrontTask = Lab7_Frontend(1)
    
    while True:
        FrontTask.run()
