##  @file Lab6Backend.py
#   Documentation for Lab6Backend.py, which contains an FSM that is in charge of receiving the desired Kp from the Lab6Frontend.py file. Once Kp is read, the backend creates a reference omega
#   of 800 rpm, which is sent bit-by-bit to the Controller_Task.py file along with the Kp value. This file also receives an array of the actual motor velocity along with it's timestamp. This data 
#   is appended and sent to the Lab6Frontend.py file.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/Lab6Backend.py
#
#   @author Keanau Robin
#
#

import shares
# import pyb
from pyb import UART
# ser=UART(2)
# ser = pyb.USB_VCP()
import utime


class Lab6_Backend:
    '''
    @brief     A finite state machine that sends the user's desired Kp from the frontend to the Controller_Task.py, and sends the motor's actual velocity from the Controller Task to the Frontend.
    @details   This class implements a finite state machine that sends the user's desired Kp from the Frontend to the Controller Task, along with a reference velocity. Then, this class takes
               the motor velocity data from the Controller Task and sends it to the Frontend for plotting. 
    '''
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT                       = 0
    
    S1_RECEIVE_USER_INPUTS        = 1
    
    S2_SEND_TO_TASK               = 2
    
    S3_RECEIVE_FROM_TASK          = 3
    
    S4_SEND_TO_FRONT              = 4
    
    
    
    def __init__(self,interval):
        
        
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        # self.next_time = self.start_time + self.interval
        
        self.Omega_refe = []
        
        self.Omega_Actual = []
        
        self.Time_Actual = []
        
        self.ser = UART(2)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
            
            elif(self.state == self.S1_RECEIVE_USER_INPUTS):
                # Run State 1 Code
                if self.ser.any() != 0:
                    Front_Inputs = self.ser.readline().decode('ascii')
                    # print('State 1 here.')
                    shares.Kp = float(Front_Inputs)
                    # Front_List = Front_Inputs.strip().split(';')
                    # Front_List = Front_Inputs
                    # print(Front_List)
                    self.transitionTo(self.S2_SEND_TO_TASK)
                else:
                    self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
                    self.runs = 0
                if self.runs >= 251:
                    self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
                    
            elif(self.state == self.S2_SEND_TO_TASK):
                # Run State 2 Code                
                self.Omega_refe = [800]*251
                shares.Om_ref = self.Omega_refe[self.runs]
                # print('Connection b/w Back and Task Complete.')
                self.transitionTo(self.S3_RECEIVE_FROM_TASK)
                
                self.runs += 1
                
            elif(self.state == self.S3_RECEIVE_FROM_TASK):
                if shares.Om_act != None and shares.Time_act != None:
                    self.Omega_Actual.append(shares.Om_act)
                    self.Time_Actual.append(shares.Time_act)
                    shares.Om_act = None
                    shares.Time_act = None
                    self.transitionTo(self.S2_SEND_TO_TASK)
                else:
                    pass
                if self.runs >= 251:
                    self.transitionTo(self.S4_SEND_TO_FRONT)
                    
            elif(self.state == self.S4_SEND_TO_FRONT):
                    
                    self.ser.write('{:};{:}\r\n'.format(str(self.Omega_Actual), str(self.Time_Actual))) 
                    self.Omega_Actual = []
                    self.Time_Actual = []
                    # self.Omega_refe = []
                    self.transitionTo(self.S1_RECEIVE_USER_INPUTS)
                    
            else:
                    # Invalid state code (error handling)
                pass
            
                # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                # self.next_time = self.next_time + self.interval


    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState            
        