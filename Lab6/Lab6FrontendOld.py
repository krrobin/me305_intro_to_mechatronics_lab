##  @file Lab6Frontend.py
#   Documentation for UI_Frontend.py, which contains two functions: sendChar and receiveArray.
#   This front end code is what the user is interfacing with in order to acheieve the data collection of the encoder position. In this code, the appended arrays of position and time
#   from the UI_dataGen.py code are plotted and saved in a CSV file named lab4plot.csv. To begin data collection, the user is prompted to type G. The user is also prompted to type S
#   if they wish to stop the data collection while it is in the process of collecting. If S is not typed, the code will continue to run for 10 seconds, and then the plot and csv file
#   will be displayed.
#
#   File can be found here: 
#
#   @author Keanau Robin
#


import serial, keyboard
import matplotlib.pyplot as plt
import time

ser = serial.Serial(port='COM5',baudrate=115273,timeout=1) # same as myuart = UART(2) but in Serial because we are dealing with serial communication in the laptop, not Nucleo

def sendUserInputs():
    '''
    @brief      This function prompts the user to input a command.
    @details    This function prompts the user to type G to begin the data collection. The ascii
                value for G will be sent to the back end and initiates the data collection.
    '''
    Kp = float(input('Please input your desired Kp value (integers only):'))
    print('Kp was inputted')
    # step_input = [800]*251     # the step input which are reference omegas
    # ser.write('{:};{:}\r\n'.format(str(Kp), str(step_input)).encode('ascii'))     # sends the input from inv to the Nucleo
    # ser.write(str(Kp).encode('ascii'))
    ser.write('{:f}\r\n'.format(Kp).encode('ascii'))
    
    
def receiveBackendArrays():
    TimeAct = []
    OmegaAct = []
    # run = 0
    # if ser.any() != 0:
    #     for run in range(501):
    #         BackendArrays = ser.readline().decode('ascii')
    #         if BackendArrays != 0:
    #             Backend_List = BackendArrays.strip().split(';')
    #             run += 1
    time.sleep(6)
    BackendArrays = ser.readline().decode('ascii')
    # run = 0
    # for run in range(1000):
        # BackendArrays = ser.readline().decode('ascii')
        
        # print('readline is complete')
        
    if BackendArrays != None:
        Backend_List = BackendArrays.strip('[]\r\n').split('];[')
        # print('Backend List is stripped and split')
        # run += 1
        OmegaActual = (Backend_List[0].split(','))
        print(OmegaActual)
        TimeActual = (Backend_List[1].split(','))
        print(TimeActual)
        for n in range (len(OmegaActual)):
            OmegaAct.append(float(OmegaActual[n]))
            TimeAct.append(float(TimeActual[n])/1000)
            
        Backend_List = None
        n = 0
    step_input = [800]*251
    plt.plot(TimeAct[1:150], OmegaAct[1:150])
    plt.plot(TimeAct[1:150], step_input[1:150])
    plt.xlabel('Time [s]')
    plt.ylabel('Shaft Velocity [rpm]')
    plt.legend(['Omega vs Time','Step Input vs Time'])
    
# for n in range(1):
if __name__ == '__main__':
    sendUserInputs()
    receiveBackendArrays()
    # mylist = receiveBackendArrays()
    print('Data Collection complete. Please rerun the file to start new data collection.')


ser.close()

