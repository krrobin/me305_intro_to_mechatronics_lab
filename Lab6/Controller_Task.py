##  @file Controller_Task.py
#   Documentation for Controller_Task.py, which contains the finite state machine that is in charge of using the user's Kp value and the Reference Velocity to run the DC motor. This
#   is done by implementing methods located in the following class files: CL_Driver.py, Motor_Driver.py, and Encoder_Class.py. The Kp value and Reference Velocity array are used to 
#   create new effort values that will take the place of the motor's new duty cycle. In the end, the actual motor velocity is collected and sent to the Lab6Backend.py file, along with
#   the timestamp for each motor velocity value. The data is being collected at 50Hz for 5 seconds.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/Controller_Task.py
#
#   @author Keanau Robin
#
#

from Encoder_Class import Encoder
from CL_Driver import ClosedLoop
from Motor_Driver import MotorDriver
import shares
import utime
# import time

class Controller:
    '''
    @brief     A finite state machine that collects the Motor Velocity data.
    @details   This class uses a finite state machine that implements methods from 3 different class files in order to collect Motor Velocity data and send it to the backend.
    '''
    
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT                 = 0
    
    S1_WAIT_FOR_Kp          = 1
    
    S2_CALC_L_and_OMEGA     = 2
    
    def __init__(self,interval,Encoder,ClosedLoop,MotorDriver):
        
        self.Encoder = Encoder
        
        self.ClosedLoop = ClosedLoop
        
        self.MotorDriver = MotorDriver
        
        self.ClosedLoop.om = 0
        
        self.new_L = 0
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*1e3)
        # self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        # self.next_time = self.start_time + self.interval
        
        # shares.Kp = 1.5
        
        # shares.Om_ref = [800]*251
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_Kp)
            
            elif(self.state == self.S1_WAIT_FOR_Kp):
                # Run State 1 Code     
                if shares.Kp != None:
                    self.ClosedLoop.set_Kp(shares.Kp)
                    self.transitionTo(self.S2_CALC_L_and_OMEGA)
                    shares.Om_act = self.ClosedLoop.om
                else:
                    self.transitionTo(self.S1_WAIT_FOR_Kp)
                    self.runs = 0
                
            elif(self.state == self.S2_CALC_L_and_OMEGA):
                # Run State 2 Code
                self.Encoder.update()
                self.ClosedLoop.om_ref = shares.Om_ref
                self.ClosedLoop.update()
                self.new_L = self.ClosedLoop.L     # updates our effort which is inputted into our set_duty method
                self.MotorDriver.enable()
                self.MotorDriver.set_duty(self.new_L)
                self.Encoder.get_delta()
                self.ClosedLoop.om = (self.Encoder.delta*60000/4000)/self.interval      # units are in rpm
                shares.Om_act = self.ClosedLoop.om
                shares.Time_act = self.interval*self.runs
                if self.interval*self.runs >= 5000:
                    self.transitionTo(self.S1_WAIT_FOR_Kp)
                    self.runs = 0
                    self.MotorDriver.disable()
                    shares.Kp = None
                    self.new_L = 0
                    shares.Om_ref = None
                else:
                    self.transitionTo(self.S2_CALC_L_and_OMEGA)
                
                self.runs += 1
                    
            else:
                    # Invalid state code (error handling)
                pass
            
                # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
                # self.next_time = self.next_time + self.interval

    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState            
        
