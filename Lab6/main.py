##  @file main.py
#   Documentation for main.py used in Lab 6 DC Motors. This file runs the Lab6Backend.py and Controller_Task.py files from the Nucleo. Two task objects are created and ran. This main.py
#   file runs in the Nucleo while the Lab6Frontend.py file runs on Spyder. Both will be running simultaneously. It is important to note that the motor pins are specified in this file.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/main.py
#
#   @author Keanau Robin
#
#


import pyb
import shares
from Encoder_Class import Encoder
from CL_Driver import ClosedLoop
from Motor_Driver import MotorDriver
from Controller_Task import Controller
from Lab6Backend import Lab6_Backend

myencoder = Encoder()
myclosedloop = ClosedLoop()
mymotor = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, pin1channel=3, pin2channel=4, timer=3)

# Creating the task objects
task1 = Controller(.02, myencoder, myclosedloop, mymotor)
task2 = Lab6_Backend(.01)

# Run the tasks in sequence over and over again
while True: # effectively while(True):
    task1.run()
    task2.run()