##  @file Lab6Frontend.py
#   Documentation for Lab6Frontend.py, which contains two functions: sendUserInputs and receiveBackendArrays. This frontend code is what the user is interfacing with in order to 
#   successfully input a Kp controller gain value into the system. This frontend file will send the desired Kp value to the Lab6backend.py file. This file also receives arrays that
#   represent actual time [s] and actual motor velocity [rpm], and then plots this data on a plot that also includes the step input line. 
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/Lab6Frontend.py
#
#   @author Keanau Robin
#

import serial
import matplotlib.pyplot as plt
import time

ser = serial.Serial(port='COM5',baudrate=115273,timeout=1) # same as myuart = UART(2) but in Serial because we are dealing with serial communication in the laptop, not Nucleo



class Lab6_Frontend:
    '''
    @brief     A finite state machine that sends a Kp to the backend, and then receives motor velocity data to plot against time.
    @details   This class implement a finite state machine that the user runs to get a plot of the DC motor's velocity for a Kp value chosen by the user. Once the user puts their
               desired Kp value on the console, that is sent to the backend which will begin the data collection. This class will receive motor velocity data and will plot that data.
    '''   
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT                       = 0
    
    S1_SEND_TO_BACKEND            = 1
    
    S2_RECEIVE_FROM_BACKEND       = 2
    
    S3_PLOT                       = 3
    
    
    
    
    def __init__(self,interval):
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        # self.interval = int(interval*1e3)
        self.interval = interval
        
        ## The timestamp for the first iteration
        # self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        # self.next_time = utime.ticks_add(self.start_time, self.interval)
        self.next_time = self.start_time + self.interval
        
        self.Kp = 0
        
        self.TimeAct = []
        
        self.OmegaAct = []
        
        self.step_input = []
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        # self.curr_time = utime.ticks_ms()
        self.curr_time = time.time()
        # if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_SEND_TO_BACKEND)
            
            elif(self.state == self.S1_SEND_TO_BACKEND):
                # Run State 1 Code
                self.sendUserInputs()
                if self.Kp != 0:
                    self.transitionTo(self.S2_RECEIVE_FROM_BACKEND)
                else:
                    self.transitionTo(self.S1_SEND_TO_BACKEND)
                
                
            elif(self.state == self.S2_RECEIVE_FROM_BACKEND):
                # Run State 2 Code
                if self.runs >= 8:
                    self.BackendArrays = ser.readline().decode('ascii')
                    if self.BackendArrays != None:
                        self.receiveBackendArrays()
                        self.transitionTo(self.S3_PLOT)
                else:
                    self.runs += 1
                    self.transitionTo(self.S2_RECEIVE_FROM_BACKEND)
                    
            elif(self.state == self.S3_PLOT):
                
                self.step_input = [800]*250
                plt.plot(self.TimeAct[1:150], self.OmegaAct[1:150])
                print(self.TimeAct)
                print(self.step_input)
                plt.plot(self.TimeAct[1:150], self.step_input[1:150])
                
                plt.xlabel('Time [s]')
                plt.ylabel('Shaft Velocity [rpm]')
                plt.legend(['Omega vs Time','Step Input vs Time'])
                self.runs = 0
                self.OmegaAct = []
                self.TimeAct = []
                self.step_input = []
            else:
                    # Invalid state code (error handling)
                pass
            
                # Specifying the next time the task will run
            # self.next_time = utime.ticks_add(self.next_time, self.interval)
            self.next_time = self.next_time + self.interval


    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
    
    def sendUserInputs(self):
        '''
        @brief      This function prompts the user to input a desired Kp value for the closed loop system.
        @details    This function prompts the user to input a desired Kp value, which will be sent to the backend to begin data collection. Decimal values are allowed.
        '''
        self.Kp = float(input('Please input your desired Kp value:'))
        print('Kp was inputted')
        ser.write('{:f}\r\n'.format(self.Kp).encode('ascii'))
    
    
    def receiveBackendArrays(self):
        '''
        @brief      This function reads the data received from the backend, and then plots that data.
        @details    This function reads the data received from the backend, and then plots te data for the user to observe the motor velocity for a given Kp value. 
        '''
        
        self.Backend_List = self.BackendArrays.strip('[]\r\n').split('];[')
        self.OmegaActual = (self.Backend_List[0].split(','))
        print(self.OmegaActual)
        self.TimeActual = (self.Backend_List[1].split(','))
        print(self.TimeActual)
        for n in range (len(self.OmegaActual)):
            self.OmegaAct.append(float(self.OmegaActual[n]))
            self.TimeAct.append(float(self.TimeActual[n])/1000)
            self.Backend_List = None
    
if __name__ == '__main__':
    
    FrontTask = Lab6_Frontend(1)
    
    while True:
        
        FrontTask.run()
        
    print('Data Collection complete. Please rerun the file to start new data collection.')
    