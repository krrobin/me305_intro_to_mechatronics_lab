
##  @file Virtual_Physical_LED_Patterns.py
#   Documentation for Virtual_Physical_LED_Patterns.py, which contains code for a virtual LED and real LED on the Nucleo
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab2/Virtual_Physical_LED_Patterns.py
#
#   @author Keanau Robin
#
#

import pyb
import utime
# import time

class TaskLEDPatterns:
    '''
    @brief      A finite state machine to control a virtual LED and a real LED with a sawtooth pattern
    @details    This class implements a finite state machine to control the
                operation of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_VALUE_PRINT      = 1    
    
    ## Constant defining State 2
    S2_FINAL            = 2    
    
    
    def __init__(self, interval, section):
        '''
        @brief            Creates a TaskLEDPatterns object.
        @param section    A variable representing if we are doing the virtual or real LED
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The object that represents if we are doing virtual=1 or real=2
        self.section = section
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in milliseconds between runs of the task
        self.interval = int(interval*1e3)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, 91*self.interval)
        #self.next_time = self.start_time + (1/11)*self.interval
        
        ## The variable that is used to change the LED brightness
        self.value = 0
        
        
        self.value_s1 = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        tim2 = pyb.Timer(2, freq = 20000)
        t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
        
        self.curr_time = utime.ticks_us()
        #self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        #if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                print('Initializing LED')
                self.transitionTo(self.S1_VALUE_PRINT)
            
            elif(self.state == self.S1_VALUE_PRINT):
                # Run State 1 Code
                if(self.section==1):
                    if self.value_s1 != 11:
                        self.value_s1 += 1
                    if(self.value_s1 == 11):
                        self.transitionTo(self.S2_FINAL)
                        print('LED OFF')
                        self.value_s1 = 0
                
                elif(self.section==2):
                    if self.value != 100:
                        self.value = self.value + 10
                        t2ch1.pulse_width_percent(self.value)
                        # print(self.value)
                    else:
                        self.value=0
                        t2ch1.pulse_width_percent(self.value)
                        # print(self.value)
                    self.transitionTo(self.S2_FINAL)
                    
            
            elif(self.state == self.S2_FINAL):
                # Run State 2 Code
                if(self.section==1):
                    if self.value_s1 != 11:
                        self.value_s1 += 1
                    if(self.value_s1 == 11):
                        self.transitionTo(self.S1_VALUE_PRINT)
                        print('LED ON')
                        self.value_s1 = 0
                
                elif(self.section==2):
                    if self.value != 100:
                        self.value = self.value + 10
                        t2ch1.pulse_width_percent(self.value)
                        # print(self.value)
                    else:
                        self.value=0
                        t2ch1.pulse_width_percent(self.value)
                        # print(self.value)
                    self.transitionTo(self.S1_VALUE_PRINT)
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, 91*self.interval)
            # self.next_time = self.next_time + (1/11)*self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

