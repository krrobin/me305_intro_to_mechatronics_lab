
##  @file Elevator_main.py
#   Documentation for Elevator_main.py, which contains code for implementing the elevator tasks 
#   by setting known parameters to their equivalent pins, then independently running both tasks 1 and 2.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/HW0/Elevator_main.py
#
#   @author Keanau Robin
#
#
#


from Elevator_FSM import Button, MotorDriver, TaskElevator


# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructor
button_1 = Button('PB6')
button_2 = Button('PB7')
first = Button('PB8')
second = Button('PB9')
motor = MotorDriver()

# Creating task objects using the buttons and motor objects above
task1 = TaskElevator(0.1, button_1, button_2, first, second, motor)
task2 = TaskElevator(0.1, button_1, button_2, first, second, motor)

# Run the tasks in sequence over and over again
for N in range(10000000): # effectively while(True):
    task1.run()
    task2.run()