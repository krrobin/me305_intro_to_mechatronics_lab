 ## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is Keanau Robin's ME 305 Portfolio. It contains code for all lecture and lab assignments for ME 305.
#
#  @section sec_fibo Fibonacci Generator
#  This section contains code for a Fibonacci generator. Click Lab1Part3.py for further information.
#  Source: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab1/Lab1Part3.py
#
#
#  @section sec_elevator Elevator FSM
#  This section contains code that implements cooperative multi-tasking to simulate an elevator within a two-floor building.
#  Click Elevator_FSM.py for further information.
#  Source: 
#
#  Source for Elevator_main: 
#
#  @author Keanau Robin
#
#
#  @date September 30, 2020
#