
##  @file Lab1Part3.py
#   Documentation for Lab1Part3.py, which is code for Fibonacci Generator.
#   All user inputs must be numerical values.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab1/Lab1Part3.py
#   @author: Keanau Robin
#
#
#   There must be a docstring at the beginning of a Python source file
#   with an @file [filename] tag in it!

def fib (idx):
    '''This method calculates a Fibonacci number corresponding to
    a specified index.
    @param idx An integer specifying the index of the desired
    Fibonacci number.'''
    print ('Calculating Fibonacci number at '
           'index n = {:}.'.format(idx))
    var=True
    while var:
        UserResponse=input('Enter an index value or type exit:')
        if UserResponse.isdigit():
            idx=int(UserResponse)
            numlist=[0, 1]
            if idx==0:
                print(numlist[idx])
            elif idx==1:
                print(numlist[idx])
            elif idx>1:
                for i in range(2,idx+1):
                    numlist.append(numlist[i-1]+numlist[i-2])
                print(numlist[idx])
        elif UserResponse=='exit':
            var=False
        else:
            print('Must input positive integer value')

if __name__ == '__main__':
    ''' Replace the following statement with the user interface code
    that will allow testing of your Fibonnaci function. Any code
    within the if __name__ == '__main__' block will only run when
    the script is executed as a standalone program. If the script
    is imported as a module the code block will not run.'''
fib(1)
