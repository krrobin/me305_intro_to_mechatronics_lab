##  @file UI_Frontend.py
#   Documentation for UI_Frontend.py, which contains two functions: sendChar and receiveArray.
#   This front end code is what the user is interfacing with in order to acheieve the data collection of the encoder position. In this code, the appended arrays of position and time
#   from the UI_dataGen.py code are plotted and saved in a CSV file named lab4plot.csv. To begin data collection, the user is prompted to type G. The user is also prompted to type S
#   if they wish to stop the data collection while it is in the process of collecting. If S is not typed, the code will continue to run for 10 seconds, and then the plot and csv file
#   will be displayed.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab4/UI_Frontend.py
#
#   @author Keanau Robin
#

import serial, keyboard
import matplotlib.pyplot as plt
import numpy as np


ser = serial.Serial(port='COM5',baudrate=115273,timeout=1) # same as myuart = UART(2) but in Serial because we are dealing with serial communication in the laptop, not Nucleo

def sendChar():
    '''
    @brief      This function prompts the user to input a command.
    @details    This function prompts the user to type G to begin the data collection. The ascii
                value for G will be sent to the back end and initiates the data collection.
    '''

    inv = input('Please type G to begin collecting data, or type S to stop collecting data:')
    ser.write(str(inv).encode('ascii'))     # sends the input from inv to the Nucleo


time = []
pos = []
def receiveArray():
    '''
    @brief      This function retrieves the array from the UI_dataGen.py file.
    @details    This function takes the data from the UI_dataGen.py and formats the data so it can be plotted and saved in a csv file.
    '''
    run = 0
    for run in range(11):
        line_string = ser.readline().decode('ascii')  # read a single line of text
        if line_string != 0:
            line_list = line_string.strip().split(',')  # strip any special characters and then split the string into wherever a comma appears; 
                # this will create a new list with two strings
            run += 1
            print('Collecting Data...')
            print(line_list)
        if keyboard.is_pressed('S'):  # for keyboard to work, you must input "pip3 install keyboard" in your command prompt
            ser.write('S'.encode('ascii'))
            print('Data Collection has been cancelled')
            break
    return line_list


sendChar()
mylist = receiveArray()
print('Data Collection complete. Please rerun the file to start new data collection.')
plt.plot(mylist[1:50],mylist[52:101]) # makes a plot for position vs time
plt.xlabel('Time [s]')
plt.ylabel('Position [deg]')
plt.show()
np.savetxt('lab4plot.csv', mylist, fmt = '%s', delimiter = ',')
ser.close()

