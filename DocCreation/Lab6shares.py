
## @file Lab6shares.py
#  @brief A container for all the inter-task variables
#  @author Keanau Robin
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/shares.py

## The command character sent from the user interface task to the cipher task
# cmd     = None
Kp       = None
Om_ref   = None

## The response from the cipher task after encoding
# rsp     = None
Om_act   = None
Time_act = None