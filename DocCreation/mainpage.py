 ## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is Keanau Robin's ME 305 Portfolio. It contains code for all lecture and lab assignments for ME 305.
#
#  @section sec_fibo Fibonacci Generator
#  This section contains code for a Fibonacci generator. Click Lab1Part3.py for further information.
#  Source: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab1/Lab1Part3.py
#
#
#  @section sec_elevator Elevator FSM
#  This section contains code that implements cooperative multi-tasking to simulate an elevator within a two-floor building.
#  Click Elevator_FSM.py for further information.
#
#  Source: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/HW0/Elevator_FSM.py
#
#  Source for Elevator_main: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/HW0/Elevator_main.py
#
#  @image html Elevator_StateTransitionDiagram.png
#
#
#  @section sec_LED LED Patterns FSM
#  This section contains code that implements cooperative multi-tasking to simulate a virtual LED and run a real LED on the Nucleo.
#  I want to quickly note that collaboration with Ashley Humpal and Kyle Chuang was done during this lab.
#
#  Click Virtual_Physical_LED_Patterns.py for further information.
#
#  Source: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab2/Virtual_Physical_LED_Patterns.py
#
#  Source for Virtual_Physical_LED_Patterns_main: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab2/Virtual_Physical_LED_Patterns_main.py
#
#  @image html LED_Pattern_StateTransitionDiagram.png
# 
#
#  @section sec_Encoder Encoder and UI
#  This section contains code that implements cooperative multi-tasking to simulate a virtual LED and run a real LED on the Nucleo.
#  I want to quickly note that collaboration with Ashley Humpal and Kyle Chuang was done during this lab.
#
#  Click Encoder_Class.py and FSM_Encoder.py for further information on the Encoder.
#
#  Click UI_Encoder.py for further information on the User Interface.
#
#  Source for Encoder FSM: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/FSM_Encoder.py
#
#  Source for Encoder Class: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/Encoder_Class.py
#
#  Source for Encoder Main: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/Encoder_main.py
#
#  Source for User Interface FSM: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab3/UI_Encoder.py
#
#  @image html State_Transition_Encoder.PNG
#
#  @image html State_Transition_UI_Encoder.PNG
#
#
# @section sec_DataCollection Data Collection
# This section contains files for the Lab 4 Data Collection process. The combined files (i.e. UI_dataGen.py, Encoder_Class.py, UI_Frontend.py, and main.py) allows the user to collect
# data on the encoder's position in degrees for a 10 second time span. The data is collected at a frequency of 5 Hz. The user is prompted to type G to begin data collection. Once
# data collection has begun, it will only end once the 10 seconds has past, or if the user types S to stop data collection.
# I want to quickly note that collaboration with Ashley Humpal and Kyle Chuang was done during this lab.
#
# Click UI_dataGen.py for further information on the back end of this lab.
#
# Click UI_Frontend.py for further information on the user interface involved in this lab.
#
# Source code is also located here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab4/
#
#  @image html DataCollectionStateDiagram.PNG
#
#  @image html DataCollectionTaskDiagram.PNG
#
#
# @section sec_BluetoothInterFace BLE Interface and App Inventor
# This section contains files for the Lab 5 BLE Interface and App Creation process. The combined files (i.e. BLE_Task.py and BLE_Driver.py) allow the user to input their desired 
# LED blinking frequency and implement that desired value onto the LED (LD2) located on the Nucleo board. In the BLE_Driver.py file, the LED pin and UART are initialized. Along
# Along with that, the functions required within the BLE_Task.py are defined in this file as well. In the BLE_Task,py file, a FSM is introduced and used to take the user's desired
# frequency, which is used to update the blinking frequency of the LED on the Nucleo. A project file (.aia file) of the Android app is included in the source code link below.
#
# It should be noted that the BLE_Task.py file was not working due to a TypeError that displayed in Putty. The FRQ value in the task file refused to change from a NoneType variable 
# to an actual value. Because of this, I made a separate task file called BLE_CheatTest.py to run the lab without the use of the BLE_Driver.py file. This allowed me to check if my
# phone app worked properly, as well as to check if the connection between the phone app and the board was working properly. The BLE_CheatTest.py file can be found in the source
# code below as well.
#
# Click BLE_Task.py for further information on the FSM of this lab.
#
# Click BLE_Driver.py for further information on the driver file of this lab.
#
# Source code is located here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab5/
# 
# Below are the figures for the State Transition Diagram, Task Diagram, and the Phone App display from the MIT App Inventor.
#  @image html BTStateDiagram.PNG
#
#  @image html BTTaskDiagram.PNG
#
#  @image html BTPhoneApp.PNG
#
#
# @section sec_MotorDriver Motor Driver
# This section contains files for the Lab 6 DC Motor process. This lab includes the following files: Lab6Motor_Driver.py, ab6Encoder_Class.py, Lab6CL_Driver.py, Lab6Frontend.py, Lab6Backend.py, 
# Controller_Task.py, Lab6shares.py, and Lab6main.py. These files were combined to simulate a closed-loop control system that operates the CD motor located on our PCB. Once the user runs the
# Lab6Frontend.py file on Spyder with the Lab6main.py file in board, the user will be prompted to input a Kp value for the closed loop system. That Kp value will be sent to the Lab6Backend.py file.
# In the Lab6Backend.py file, the reference omega array is created. Both the reference omega array and Kp are then sent to the Controller_Task.py to start the motor, encoder, and data collection.
# Once data collection is complete, it is sent to the backend, and then eventually to the frontend for plotting. For further infomration on the process within each file, please click any of the 
# files mentioned in the beginning of this paragraph.
#
# It is important to note that the user must have the Lab6main.py file running in Putty while running the Lab6Frontend.py file on Spyder. Once the plots have been created, the user must type ser.close()
# in the console, press ctrl+C and ctrl+D in Putty to restart the Lab6main.py file, and then rerun the frontend file. The user can try to do this process without restarting the Lab6main.py file in Putty, but
# that sometimes leads to inconsistencies in the plotting. I found that it creates plots that show the omega near zero, even though it is rotating at a magnitude far from zero.
#
# It is also important to note that I collaborated with Kyle Chuang and Ashley Humpal for this lab.
#
# Source code is located here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/
#
# Below are the State Transition Diagrams and Task Diagram explaining the process of Lab 6.
#
# @image html Lab6FrontendSD.PNG
#
# @image html Lab6BackendSD.PNG
#
# @image html Lab6ControllerSD.PNG
#
# @image html Lab6TaskDiagram.PNG
#
#
# Below are images of the Motor Velocity vs Time plots created after choosing three different Kp values. Each plot includes the reference velocity as well. The first plot corresponds to a Kp of 0.2.
# The second plot corresponds to a Kp of 0.5. The last plot corresponds to a Kp of 1.0.
#
# @image html Kp02trial2.png
#
# @image html Kp05trial2.png
#
# @image html Kp1.png
#
#
# @section sec_ReferenceTracking Reference Tracking
# This section contains files for the Lab 7 Reference Tracking. This lab includes the following files: Lab7Motor_Driver.py, Lab7Encoder_Class.py, Lab7CL_Driver.py, Lab7Frontend.py, Lab7Backend.py, 
# Controller_TaskLab7.py, Lab7shares.py, and Lab7main.py. This lab uses the code from Lab 6 DC Motors, but the files were updated to accept reference data (i.e. reference velocity, reference 
# position, and the timestamps corresponding to those) given in the excel files UpdatedRef.csv and UpdatedRefBackend.csv. In terms of edits, there were some updates on the frontend, backend, and 
# controller task files. To see the updated description, please click Lab7Frontend.py, Lab7Backend.py, and Controller_TaskLab7.py for more information. These edits were mainly done to allow the files
# to accept the new reference data and also to include position data now.
#
# It is important to note that the user must have the Lab7main.py file running in Putty while running the Lab7Frontend.py file on Spyder. Once the plot has been created, the user is prompted to stop
# the current command and type in ser.close() in the console. After that, the user must rerun the frontend and main file to do a new plot. If the main file is not restarted, the user will get an error
# which will prevent the creation of the plot. 
#
# It is also important to note that I collaborated with Kyle Chuang and Ashley Humpal for this lab.
#
# Source code is located here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab7/
#
# Below is the new State Transition Diagram for the Frontend of Lab 7. For the other State Transition Diagrams and Task Diagram, they are similar to the Lab 6 diagrams, so please refer to the previous
# lab's state and task diagrams.
#
# @image html Lab7FrontendSD.PNG
#
# Below is the figure of the two subplots generated by the frontend. The first subplot includes the reference veloicty and actual velocity. THe second subplot includes the reference position and actual 
# position. The reference profiles were scaled higher than the original reference values because the original reference velocities were far too small and resulted in my motors not running. So, I mutliplied the
# reference values (velocity and position) by a value of 15. Also, I updated the data to only include every 50ms of data. And finally, I only included 14 seconds of data, as opposed to the full 15 seconds
# to prevent a memory allocation error that I was having. Plus, the last second of data only had the motor staying stationary, so it didn't seem like crucial data.
#
# @image html Lab7Plot06.png
#
# The Kp in this figure ws 0.6. I started with a value of 0.5, but that gave me a J of approximately 13*10^6. The Kp of 0.6 gave me a J of approximately 10*10^6. Other Kp's above that would result in unwanted
# oscillations within my motor, which ruined the data. So, I stayed with the Kp of 0.6. 
#
#  @author Keanau Robin
#
#
#  @date December 4, 2020
#