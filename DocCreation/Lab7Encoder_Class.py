##  @file Lab7Encoder_Class.py
#   Documentation for Lab7Encoder_Class.py, which contains code that defines all the Encoder methods required for our DC Motor lab to function properly.
#   These methods include update(), get_position(), set_position(), and get_delta().
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab7/Encoder_Class.py
#
#   @author Keanau Robin
#
#

import pyb


class Encoder:
    '''
    @brief      Encoder class with all the methods for running the Encoder task.
    @details    This class defines all the methods required for the user to properly run the Encoder. These methods are used in the Encoder FSM.
    '''
    
    def __init__(self):
        '''
        @brief Creates an Encoder Object
        '''

        self.period=0xFFFF
        
        self.delta = 0
        
        self.position = 0
        
        self.raw_delta = 0
        
        self.original_pos = 0
        
        
        # self.tim = pyb.Timer(3)
        # self.tim.init(prescaler=3, period = self.period)
        # self.tim.channel(1, pin=pyb.Pin.cpu.A6, mode=pyb.Timer.ENC_AB)
        # self.tim.channel(2, pin=pyb.Pin.cpu.A7, mode=pyb.Timer.ENC_AB)
        
        self.tim = pyb.Timer(8)
        self.tim.channel(1,pin=pyb.Pin.cpu.C6, mode=pyb.Timer.ENC_AB)
        self.tim.channel(2,pin=pyb.Pin.cpu.C7, mode=pyb.Timer.ENC_AB)
        self.tim.init(prescaler=3, period = self.period)
        
        
        pass
        
    def update(self):
        '''
        @brief   Updates the recorded position of the encoder
        '''
        
        self.updated_pos = self.tim.counter()
        
        self.raw_delta =self.updated_pos - self.original_pos
        
        self.original_pos = self.updated_pos
        
        if abs(self.raw_delta)>0.5*self.period:
            # Considered a bad delta if this if-statement is true
            if self.raw_delta<0:
                self.delta = self.raw_delta + self.period
            elif self.raw_delta>0:
                self.delta = self.raw_delta - self.period
        else:
            self.delta = self.raw_delta
        
        self.position = self.position + self.delta
        
        
    def get_position(self):
        '''
        @brief   Returns the most recently updated position of the encoder
        '''
        return self.position
    
    def set_position(self,desired_val):
        '''
        @brief   Resets the position to a specified value
        '''
        self.position = desired_val
        return self.position
        

    def get_delta(self):
        '''
        @breif   Returns the difference in recorded position between the two most recent calls to update()
        '''
        return self.delta

