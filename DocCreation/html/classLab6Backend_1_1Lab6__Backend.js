var classLab6Backend_1_1Lab6__Backend =
[
    [ "__init__", "classLab6Backend_1_1Lab6__Backend.html#a3fdd08d7c3f1d74b8bfe619bbecfdfe9", null ],
    [ "run", "classLab6Backend_1_1Lab6__Backend.html#a12f9b24fe2b51dbd26ce77fe2786bd7b", null ],
    [ "transitionTo", "classLab6Backend_1_1Lab6__Backend.html#ab02e0f1b24644983829be93e74c863de", null ],
    [ "curr_time", "classLab6Backend_1_1Lab6__Backend.html#a31e2551908c5a5420c0c4454f6064aac", null ],
    [ "interval", "classLab6Backend_1_1Lab6__Backend.html#af3e5db8e7a93ca67607bbae81556eec8", null ],
    [ "next_time", "classLab6Backend_1_1Lab6__Backend.html#ad7b52c23c2713a01c8ee8738a054a24f", null ],
    [ "Omega_Actual", "classLab6Backend_1_1Lab6__Backend.html#ab05b9b02d38660fe22ddd220aa0e8c07", null ],
    [ "Omega_refe", "classLab6Backend_1_1Lab6__Backend.html#a173ab04456f8c950aafd7f924ffd65c9", null ],
    [ "runs", "classLab6Backend_1_1Lab6__Backend.html#a87d18fbd77bcf115b3872711d00da7a5", null ],
    [ "ser", "classLab6Backend_1_1Lab6__Backend.html#a3898437bd838701ef207d19d87236eac", null ],
    [ "start_time", "classLab6Backend_1_1Lab6__Backend.html#ae6258b5853c75442541697a7080eb865", null ],
    [ "state", "classLab6Backend_1_1Lab6__Backend.html#a2b231b36702928e11949461597589da0", null ],
    [ "Time_Actual", "classLab6Backend_1_1Lab6__Backend.html#ae8efe544c85bb0e36932b77ea8a1761c", null ]
];