var files_dup =
[
    [ "BLE_CheatTest.py", "BLE__CheatTest_8py.html", "BLE__CheatTest_8py" ],
    [ "BLE_Driver.py", "BLE__Driver_8py.html", [
      [ "BLE_Drive", "classBLE__Driver_1_1BLE__Drive.html", "classBLE__Driver_1_1BLE__Drive" ]
    ] ],
    [ "BLE_Task.py", "BLE__Task_8py.html", "BLE__Task_8py" ],
    [ "Controller_Task.py", "Controller__Task_8py.html", [
      [ "Controller", "classController__Task_1_1Controller.html", "classController__Task_1_1Controller" ]
    ] ],
    [ "Controller_TaskLab7.py", "Controller__TaskLab7_8py.html", [
      [ "Controller", "classController__TaskLab7_1_1Controller.html", "classController__TaskLab7_1_1Controller" ]
    ] ],
    [ "Elevator_FSM.py", "Elevator__FSM_8py.html", [
      [ "TaskElevator", "classElevator__FSM_1_1TaskElevator.html", "classElevator__FSM_1_1TaskElevator" ],
      [ "Button", "classElevator__FSM_1_1Button.html", "classElevator__FSM_1_1Button" ],
      [ "MotorDriver", "classElevator__FSM_1_1MotorDriver.html", "classElevator__FSM_1_1MotorDriver" ]
    ] ],
    [ "Elevator_main.py", "Elevator__main_8py.html", "Elevator__main_8py" ],
    [ "Encoder_main.py", "Encoder__main_8py.html", "Encoder__main_8py" ],
    [ "FSM_Encoder.py", "FSM__Encoder_8py.html", [
      [ "TaskEncoder", "classFSM__Encoder_1_1TaskEncoder.html", "classFSM__Encoder_1_1TaskEncoder" ]
    ] ],
    [ "Lab1Part3.py", "Lab1Part3_8py.html", "Lab1Part3_8py" ],
    [ "Lab6Backend.py", "Lab6Backend_8py.html", [
      [ "Lab6_Backend", "classLab6Backend_1_1Lab6__Backend.html", "classLab6Backend_1_1Lab6__Backend" ]
    ] ],
    [ "Lab6CL_Driver.py", "Lab6CL__Driver_8py.html", [
      [ "ClosedLoop", "classLab6CL__Driver_1_1ClosedLoop.html", "classLab6CL__Driver_1_1ClosedLoop" ]
    ] ],
    [ "Lab6Encoder_Class.py", "Lab6Encoder__Class_8py.html", [
      [ "Encoder", "classLab6Encoder__Class_1_1Encoder.html", "classLab6Encoder__Class_1_1Encoder" ]
    ] ],
    [ "Lab6Frontend.py", "Lab6Frontend_8py.html", "Lab6Frontend_8py" ],
    [ "Lab6main.py", "Lab6main_8py.html", "Lab6main_8py" ],
    [ "Lab6Motor_Driver.py", "Lab6Motor__Driver_8py.html", [
      [ "MotorDriver", "classLab6Motor__Driver_1_1MotorDriver.html", "classLab6Motor__Driver_1_1MotorDriver" ]
    ] ],
    [ "Lab6shares.py", "Lab6shares_8py.html", "Lab6shares_8py" ],
    [ "Lab7Backend.py", "Lab7Backend_8py.html", [
      [ "Lab7_Backend", "classLab7Backend_1_1Lab7__Backend.html", "classLab7Backend_1_1Lab7__Backend" ]
    ] ],
    [ "Lab7CL_Driver.py", "Lab7CL__Driver_8py.html", [
      [ "ClosedLoop", "classLab7CL__Driver_1_1ClosedLoop.html", "classLab7CL__Driver_1_1ClosedLoop" ]
    ] ],
    [ "Lab7Encoder_Class.py", "Lab7Encoder__Class_8py.html", [
      [ "Encoder", "classLab7Encoder__Class_1_1Encoder.html", "classLab7Encoder__Class_1_1Encoder" ]
    ] ],
    [ "Lab7Frontend.py", "Lab7Frontend_8py.html", "Lab7Frontend_8py" ],
    [ "Lab7main.py", "Lab7main_8py.html", "Lab7main_8py" ],
    [ "Lab7Motor_Driver.py", "Lab7Motor__Driver_8py.html", [
      [ "MotorDriver", "classLab7Motor__Driver_1_1MotorDriver.html", "classLab7Motor__Driver_1_1MotorDriver" ]
    ] ],
    [ "Lab7shares.py", "Lab7shares_8py.html", "Lab7shares_8py" ],
    [ "UI_dataGen.py", "UI__dataGen_8py.html", "UI__dataGen_8py" ],
    [ "UI_Encoder.py", "UI__Encoder_8py.html", [
      [ "UserInterface", "classUI__Encoder_1_1UserInterface.html", "classUI__Encoder_1_1UserInterface" ]
    ] ],
    [ "UI_Frontend.py", "UI__Frontend_8py.html", "UI__Frontend_8py" ],
    [ "Virtual_Physical_LED_Patterns.py", "Virtual__Physical__LED__Patterns_8py.html", [
      [ "TaskLEDPatterns", "classVirtual__Physical__LED__Patterns_1_1TaskLEDPatterns.html", "classVirtual__Physical__LED__Patterns_1_1TaskLEDPatterns" ]
    ] ],
    [ "Virtual_Physical_LED_Patterns_main.py", "Virtual__Physical__LED__Patterns__main_8py.html", "Virtual__Physical__LED__Patterns__main_8py" ]
];