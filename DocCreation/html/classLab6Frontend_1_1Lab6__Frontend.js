var classLab6Frontend_1_1Lab6__Frontend =
[
    [ "__init__", "classLab6Frontend_1_1Lab6__Frontend.html#ac52ae8a29736bcc3e63fdbab2a916e3f", null ],
    [ "receiveBackendArrays", "classLab6Frontend_1_1Lab6__Frontend.html#a0cf8e3d60bb6faf39fa18f3c664aa192", null ],
    [ "run", "classLab6Frontend_1_1Lab6__Frontend.html#a1ada6c00d7ab46037308a74793307617", null ],
    [ "sendUserInputs", "classLab6Frontend_1_1Lab6__Frontend.html#a6bd591f887abd288a0b4f8f2565269a8", null ],
    [ "transitionTo", "classLab6Frontend_1_1Lab6__Frontend.html#a19048158642d96828e2e38b5176b9c22", null ],
    [ "Backend_List", "classLab6Frontend_1_1Lab6__Frontend.html#add8d9f74f2bdc2c9f3384af785af2761", null ],
    [ "BackendArrays", "classLab6Frontend_1_1Lab6__Frontend.html#a37d40875caab9fe60a33e42f678d35de", null ],
    [ "curr_time", "classLab6Frontend_1_1Lab6__Frontend.html#ab844b1029f7d09d59fc38238322111e9", null ],
    [ "interval", "classLab6Frontend_1_1Lab6__Frontend.html#acbf849dd0440bbaa5226cb7dac8dad4a", null ],
    [ "Kp", "classLab6Frontend_1_1Lab6__Frontend.html#ada0e42ebd8b819771842a36ca00f92b8", null ],
    [ "next_time", "classLab6Frontend_1_1Lab6__Frontend.html#a032b8675622879cd9601c8b57e516987", null ],
    [ "OmegaAct", "classLab6Frontend_1_1Lab6__Frontend.html#af7c743a951967856a7ec2c23be680349", null ],
    [ "OmegaActual", "classLab6Frontend_1_1Lab6__Frontend.html#a3091ada11f44f0395ce629e8c60180c5", null ],
    [ "runs", "classLab6Frontend_1_1Lab6__Frontend.html#aa6f4f38ae57ca6cb1d55d21c5544aff8", null ],
    [ "start_time", "classLab6Frontend_1_1Lab6__Frontend.html#a3a0e4868c66f374b3c01484ca3124057", null ],
    [ "state", "classLab6Frontend_1_1Lab6__Frontend.html#af183aa647cbaf722923aee2a95b259ff", null ],
    [ "step_input", "classLab6Frontend_1_1Lab6__Frontend.html#abdc6ae3eaed412b66f8a6e701ce6903b", null ],
    [ "TimeAct", "classLab6Frontend_1_1Lab6__Frontend.html#aa62fb777f57e48b737895dfe61635ea2", null ],
    [ "TimeActual", "classLab6Frontend_1_1Lab6__Frontend.html#ab8d104c81cbe120db2e936e5ef720c20", null ]
];