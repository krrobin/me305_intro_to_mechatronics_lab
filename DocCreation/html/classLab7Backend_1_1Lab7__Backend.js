var classLab7Backend_1_1Lab7__Backend =
[
    [ "__init__", "classLab7Backend_1_1Lab7__Backend.html#a5ae4e5d25e4d838b7f18c727837a42db", null ],
    [ "run", "classLab7Backend_1_1Lab7__Backend.html#ae05c1f75d3b615f872fdcaa823a254ed", null ],
    [ "transitionTo", "classLab7Backend_1_1Lab7__Backend.html#a370b2bdba06c6b722ce21fbefe9bca65", null ],
    [ "curr_time", "classLab7Backend_1_1Lab7__Backend.html#aba50997e45544bb0790b16cc2f56a2a8", null ],
    [ "interval", "classLab7Backend_1_1Lab7__Backend.html#a21651210d18521b0408801d52d183380", null ],
    [ "line", "classLab7Backend_1_1Lab7__Backend.html#a59ea4d4d09f0fc1be6bf6ce9cddda215", null ],
    [ "next_time", "classLab7Backend_1_1Lab7__Backend.html#a5dcdec9c51ea7e8ca3b52cb3208f8233", null ],
    [ "Omega_Actual", "classLab7Backend_1_1Lab7__Backend.html#a96006802cc6eeacf9cc835814b22f0d9", null ],
    [ "Position_Actual", "classLab7Backend_1_1Lab7__Backend.html#a79cab5968c57de9eed8f241c30806682", null ],
    [ "ref", "classLab7Backend_1_1Lab7__Backend.html#a3ae0f551229f5ee24942c65a338a4f5b", null ],
    [ "ref_velocity", "classLab7Backend_1_1Lab7__Backend.html#af61a7b7fcda758acf4399b27b6b59f09", null ],
    [ "runs", "classLab7Backend_1_1Lab7__Backend.html#a27b75b2683f7ac7a7dabb7c915243c4a", null ],
    [ "ser", "classLab7Backend_1_1Lab7__Backend.html#ae6238e76a9bd7496ce49a9bedd431988", null ],
    [ "start_time", "classLab7Backend_1_1Lab7__Backend.html#a3d8f64bd27fea7cf760aee1b562a405f", null ],
    [ "state", "classLab7Backend_1_1Lab7__Backend.html#a87acb331d0815b218c0275e8cfb00810", null ],
    [ "Time_Actual", "classLab7Backend_1_1Lab7__Backend.html#af67f6bf9f2591ff09a7e9f8f4f344be2", null ]
];