var annotated_dup =
[
    [ "BLE_CheatTest", null, [
      [ "BLE_Task", "classBLE__CheatTest_1_1BLE__Task.html", "classBLE__CheatTest_1_1BLE__Task" ]
    ] ],
    [ "BLE_Driver", null, [
      [ "BLE_Drive", "classBLE__Driver_1_1BLE__Drive.html", "classBLE__Driver_1_1BLE__Drive" ]
    ] ],
    [ "BLE_Task", null, [
      [ "BLE_Task", "classBLE__Task_1_1BLE__Task.html", "classBLE__Task_1_1BLE__Task" ]
    ] ],
    [ "Controller_Task", null, [
      [ "Controller", "classController__Task_1_1Controller.html", "classController__Task_1_1Controller" ]
    ] ],
    [ "Controller_TaskLab7", null, [
      [ "Controller", "classController__TaskLab7_1_1Controller.html", "classController__TaskLab7_1_1Controller" ]
    ] ],
    [ "Elevator_FSM", null, [
      [ "Button", "classElevator__FSM_1_1Button.html", "classElevator__FSM_1_1Button" ],
      [ "MotorDriver", "classElevator__FSM_1_1MotorDriver.html", "classElevator__FSM_1_1MotorDriver" ],
      [ "TaskElevator", "classElevator__FSM_1_1TaskElevator.html", "classElevator__FSM_1_1TaskElevator" ]
    ] ],
    [ "FSM_Encoder", null, [
      [ "TaskEncoder", "classFSM__Encoder_1_1TaskEncoder.html", "classFSM__Encoder_1_1TaskEncoder" ]
    ] ],
    [ "Lab6Backend", null, [
      [ "Lab6_Backend", "classLab6Backend_1_1Lab6__Backend.html", "classLab6Backend_1_1Lab6__Backend" ]
    ] ],
    [ "Lab6CL_Driver", null, [
      [ "ClosedLoop", "classLab6CL__Driver_1_1ClosedLoop.html", "classLab6CL__Driver_1_1ClosedLoop" ]
    ] ],
    [ "Lab6Encoder_Class", null, [
      [ "Encoder", "classLab6Encoder__Class_1_1Encoder.html", "classLab6Encoder__Class_1_1Encoder" ]
    ] ],
    [ "Lab6Frontend", null, [
      [ "Lab6_Frontend", "classLab6Frontend_1_1Lab6__Frontend.html", "classLab6Frontend_1_1Lab6__Frontend" ]
    ] ],
    [ "Lab6Motor_Driver", null, [
      [ "MotorDriver", "classLab6Motor__Driver_1_1MotorDriver.html", "classLab6Motor__Driver_1_1MotorDriver" ]
    ] ],
    [ "Lab7Backend", null, [
      [ "Lab7_Backend", "classLab7Backend_1_1Lab7__Backend.html", "classLab7Backend_1_1Lab7__Backend" ]
    ] ],
    [ "Lab7CL_Driver", null, [
      [ "ClosedLoop", "classLab7CL__Driver_1_1ClosedLoop.html", "classLab7CL__Driver_1_1ClosedLoop" ]
    ] ],
    [ "Lab7Encoder_Class", null, [
      [ "Encoder", "classLab7Encoder__Class_1_1Encoder.html", "classLab7Encoder__Class_1_1Encoder" ]
    ] ],
    [ "Lab7Frontend", null, [
      [ "Lab7_Frontend", "classLab7Frontend_1_1Lab7__Frontend.html", "classLab7Frontend_1_1Lab7__Frontend" ]
    ] ],
    [ "Lab7Motor_Driver", null, [
      [ "MotorDriver", "classLab7Motor__Driver_1_1MotorDriver.html", "classLab7Motor__Driver_1_1MotorDriver" ]
    ] ],
    [ "UI_dataGen", null, [
      [ "DataGen", "classUI__dataGen_1_1DataGen.html", "classUI__dataGen_1_1DataGen" ]
    ] ],
    [ "UI_Encoder", null, [
      [ "UserInterface", "classUI__Encoder_1_1UserInterface.html", "classUI__Encoder_1_1UserInterface" ]
    ] ],
    [ "Virtual_Physical_LED_Patterns", null, [
      [ "TaskLEDPatterns", "classVirtual__Physical__LED__Patterns_1_1TaskLEDPatterns.html", "classVirtual__Physical__LED__Patterns_1_1TaskLEDPatterns" ]
    ] ]
];