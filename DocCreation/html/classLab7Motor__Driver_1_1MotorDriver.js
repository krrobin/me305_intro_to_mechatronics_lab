var classLab7Motor__Driver_1_1MotorDriver =
[
    [ "__init__", "classLab7Motor__Driver_1_1MotorDriver.html#a3c543cb2a03cb4d9b7ac14c1c8fc3e3a", null ],
    [ "disable", "classLab7Motor__Driver_1_1MotorDriver.html#a5356e0fc1abf8b44bb7f58c0197a78fc", null ],
    [ "enable", "classLab7Motor__Driver_1_1MotorDriver.html#a0c71087253e4f1cf888ff200956bfaf9", null ],
    [ "set_duty", "classLab7Motor__Driver_1_1MotorDriver.html#a7b25dea3f95d25138c2de26c23b5fff3", null ],
    [ "nSLEEP_pin", "classLab7Motor__Driver_1_1MotorDriver.html#a095f5e6746ae7beed2981cb8b5aba962", null ],
    [ "pin1", "classLab7Motor__Driver_1_1MotorDriver.html#ad05d03b4be1250f4cc608f9ff69619be", null ],
    [ "pin1channel", "classLab7Motor__Driver_1_1MotorDriver.html#ae527c342362ce25a1489504d3ae7fbd3", null ],
    [ "pin2", "classLab7Motor__Driver_1_1MotorDriver.html#aa4ce2c530063903a2c1948dce17c760d", null ],
    [ "pin2channel", "classLab7Motor__Driver_1_1MotorDriver.html#a8b06906ff8644fceaec83a537c705315", null ],
    [ "pinSLEEP", "classLab7Motor__Driver_1_1MotorDriver.html#a5131a8b7e93b035273314ada1cf02df6", null ],
    [ "tch1", "classLab7Motor__Driver_1_1MotorDriver.html#afba35a381ac7a5b610b65e0e3b499ff1", null ],
    [ "tch2", "classLab7Motor__Driver_1_1MotorDriver.html#a363de1fb6df8305633a82f28d1ce4d99", null ],
    [ "tim", "classLab7Motor__Driver_1_1MotorDriver.html#a4f4420b040217290d4020a97ccd4f4a7", null ]
];