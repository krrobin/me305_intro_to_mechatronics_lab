var searchData=
[
  ['sendchar_159',['sendChar',['../UI__Frontend_8py.html#a30a885c0480aef016f027f0ddaf5fd23',1,'UI_Frontend']]],
  ['senduserinputs_160',['sendUserInputs',['../classLab6Frontend_1_1Lab6__Frontend.html#a6bd591f887abd288a0b4f8f2565269a8',1,'Lab6Frontend.Lab6_Frontend.sendUserInputs()'],['../classLab7Frontend_1_1Lab7__Frontend.html#a42a45df97f62f172854abf1a09104d8c',1,'Lab7Frontend.Lab7_Frontend.sendUserInputs()']]],
  ['set_5fduty_161',['set_duty',['../classLab6Motor__Driver_1_1MotorDriver.html#a257df826c69446fd20bd1863d221b336',1,'Lab6Motor_Driver.MotorDriver.set_duty()'],['../classLab7Motor__Driver_1_1MotorDriver.html#a7b25dea3f95d25138c2de26c23b5fff3',1,'Lab7Motor_Driver.MotorDriver.set_duty()']]],
  ['set_5fkp_162',['set_Kp',['../classLab6CL__Driver_1_1ClosedLoop.html#ac3828a3c0c68c975132c9bc64f659725',1,'Lab6CL_Driver.ClosedLoop.set_Kp()'],['../classLab7CL__Driver_1_1ClosedLoop.html#a35a9881eeeb10a0677e040466cd9bdf7',1,'Lab7CL_Driver.ClosedLoop.set_Kp()']]],
  ['set_5fposition_163',['set_position',['../classLab6Encoder__Class_1_1Encoder.html#a5b890e0318ce1ec9f74c65126af72ad4',1,'Lab6Encoder_Class.Encoder.set_position()'],['../classLab7Encoder__Class_1_1Encoder.html#addb6c97a564fe9eacd5d8efca411bd37',1,'Lab7Encoder_Class.Encoder.set_position()']]],
  ['stop_164',['Stop',['../classElevator__FSM_1_1MotorDriver.html#a6578c25b68c968c3ff1d503e686247a2',1,'Elevator_FSM::MotorDriver']]]
];
