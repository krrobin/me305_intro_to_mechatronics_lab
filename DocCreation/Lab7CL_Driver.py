##  @file Lab7CL_Driver.py
#   Documentation for Lab7CL_Driver.py, which contains code that encapsulates all of the features associated with a closed-loop controller for
#   the CLosed Loop system. The methods in this driver file include update(), get_Kp(), and set_Kp(). The main purpose of this driver is to
#   take a Kp controller gain, omega reference, and actual omega of the motor to solve for the effort of the motor. That effort value will
#   be used as the new duty cycle in the set_duty method within the Motor_Driver.py file. 
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab7/CL_Driver.py
#
#   @author Keanau Robin
#
#


class ClosedLoop:
    '''
    @brief      ClosedLoop class with all the methods for solving for motor effort.
    @details    This class defines all the methods for solving for motor effort, getting current Kp value, and resetting the Kp value.
    '''
    
    def __init__(self):

        self.Kp = 0
        
        self.L = 0
        
        pass
        
    def update(self):
        '''
        @brief   Updates the motor effort which will be the motor's new duty cycle.
        @details This method takes the desired motor velocity, actual motor velocity, and desired Kp value to calculate the new duty cycle 
                 for the motor. This method taes into account the saturation for the effort exceeding a value of positive or negative 100.
        '''
        Kp_prime = self.Kp/3.3
        
        self.L = Kp_prime*(self.om_ref-self.om)
        
        if self.L >= 100:
            self.L = 100
            
        elif self.L <= -100:
            self.L = -100
            
        else:
            pass
        
    def get_Kp(self):
        '''
        @brief      Returns the current Kp value.
        @details    This method returns the current Kp value that was set by the user.
        '''
        return self.Kp
        
    def set_Kp(self, desired_Kp):
        '''
        @brief      Sets the Kp value to a desired value.
        @details    This method sets the Kp value to any desired value, which is labeled as "desired_Kp".
        '''
        
        self.Kp = desired_Kp
        
        
# if __name__ == '__main__':
#     myCL = ClosedLoop()
#     myCL.Kp = 1
#     myCL.om_ref = 1
#     myCL.om = 0.6
#     myCL.update()
#     print(myCL.L)