##  @file Lab6Motor_Driver.py
#   Documentation for Motor_Driver.py, which contains code that encapsulates all of the features associated with driving the DC motor connected to the DRV8847 motor driver that is part of our base
#   unit connected to our Nucleo. The DRV8847 motor driver on our base unit is used to drive two DC motors, which are also located on our board. The features assoicated with driving the DC motors are 
#   accomplished by the methods created in this Motor_Driver.py file. Along with those methods, this file also contains test code that evaluates the functionality of the motor driver class. In this
#   test code, there are two objects of the motor driver class that can independently control the two separate motor drivers.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab6/Motor_Driver.py
#
#   @author Keanau Robin
#
#



import pyb
# from Encoder_Class import Encoder

class MotorDriver:
    '''
    @brief      This class implements a motor driver for the ME 305 board.
    @details    This class defines all the methods required to properly interact with the DC motors located on our PCB. The required methods include enable(), disable(), and set_duty(). 
    '''

    def __init__ (self, nSLEEP_pin, pin1, pin2, pin1channel, pin2channel, timer):
        ''' Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param pin1 A pyb.Pin object to use as the input to half bridge 1.
        @param pin2 A pyb.Pin object to use as the input to half bridge 2.
        @param pin1channel An object that represents the channel for pin1.
        @param pin2channel An object that represents the channel for pin2.
        @param timer A pyb.Timer object to use for PWM generation on pin1 and pin2.
        '''
        
        self.nSLEEP_pin = nSLEEP_pin
        
        self.pinSLEEP = pyb.Pin(nSLEEP_pin,pyb.Pin.OUT_PP)
        
        self.pinSLEEP.low()
        
        self.pin1 = pin1
        self.pin2 = pin2
        
        self.tim = pyb.Timer(timer, freq=20000)
        self.pin1channel = pin1channel
        self.pin2channel = pin2channel
        
        self.tch1 = self.tim.channel(self.pin1channel,pyb.Timer.PWM,pin=self.pin1)
        self.tch2 = self.tim.channel(self.pin2channel,pyb.Timer.PWM,pin=self.pin2)

    def enable (self):
        '''
        @brief      Activates the power flow by turning on pinSLEEP
        '''
        # print ('Enabling Motor')
        self.pinSLEEP.high()

    def disable (self):
        '''
        @brief      Deactivates the power flow by turning off pinSLEEP
        '''
        # print ('Disabling Motor')
        self.pinSLEEP.low()

    def set_duty (self, duty):
        '''
        @brief      This method sets the duty cycle to be sent to the motor to the given level. Positive values cause effort in one direction, negative values in the opposite direction.
        @param      duty A signed integer holding the duty cycle of the PWM signal sent to the motor
        '''

        if duty > 0:
            self.tch1.pulse_width_percent(duty)
            self.tch2.pulse_width_percent(0)
            # print('Forward Motion')
    
        elif duty < 0:
            self.tch2.pulse_width_percent(abs(duty))
            self.tch1.pulse_width_percent(0)
            # print('Reverse Motion')
        
        else:
            self.tch1.pulse_width_percent(0)
            self.tch2.pulse_width_percent(0)
            # print('Current duty cycle is 0 for both pins. Input must be a numerical nonzero value with a magnitude less than 100')
        

# if __name__ == '__main__':
#     # from Encoder_Class import Encoder
#     # Create the pin objects used for interfacing with the motor driver
#     nSLEEP_pin = pyb.Pin.cpu.A15
#     IN1_pin = pyb.Pin.cpu.B4
#     IN2_pin = pyb.Pin.cpu.B5
#     IN3_pin = pyb.Pin.cpu.B0
#     IN4_pin = pyb.Pin.cpu.B1
    
#     # Create a motor object passing in the pins and timer
#     moe1 = MotorDriver(nSLEEP_pin, pin1=IN1_pin, pin2=IN2_pin, pin1channel=1, pin2channel=2, timer=3)
    
#     moe2 = MotorDriver(nSLEEP_pin, pin1=IN3_pin, pin2=IN4_pin, pin1channel=3, pin2channel=4, timer=3)
    

#     # Enable the motor driver
#     moe1.disable()
#     moe2.enable()

#     # Set the duty cycle to 10 percent
#     moe1.set_duty(0)
#     moe2.set_duty(35)
    
#     # enc2 = Encoder()
#     # while True:
#     #     enc2.update()
#     #     enc2.get_position()
#     #     print(enc2.position)