
##  @file Virtual_Physical_LED_Patterns_main.py
#   Documentation for Virtual_Physical_LED_Patterns_main.py, which contains code for running the tasks of Virtual_Physical_LED_Patterns.py
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab2/Virtual_Physical_LED_Patterns_main.py
#
#   @author Keanau Robin
#
#

from Virtual_Physical_LED_Patterns import TaskLEDPatterns


        

# Creating the task objects
task1 = TaskLEDPatterns(10, 1)
task2 = TaskLEDPatterns(10, 2)
# Run the tasks in sequence over and over again
while True: # effectively while(True):
    task1.run()
    task2.run()
