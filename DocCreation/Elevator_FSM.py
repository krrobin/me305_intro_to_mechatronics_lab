
##  @file Elevator_FSM.py
#   Documentation for Elevator_FSM.py, which contains code for a simulated elevator within a two-story building.
#   The user has two buttons to select between floor 1 and floor 2.
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/HW0/Elevator_FSM.py
#
#   @author Keanau Robin
#
#
#



from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to simulate an elevator moving between two floors
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                   = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN            = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP              = 2    
    
    ## Constant defining State 3
    S3_STOPPED_ON_FLOOR_1     = 3    
    
    ## Constant defining State 4
    S4_STOPPED_ON_FLOOR_2     = 4
    
    ## Constant defining State 5
    S5_DO_NOTHING             = 5
    


    def __init__(self, interval, button_1, button_2, first, second, motor):
        '''
        @brief            Creates a TaskElevator object.
        @param first      An object from class Button representing first floor sensor
        @param second     An object from class Button representing second floor sensor
        @param button_1   An object from class Button representing first floor button
        @param button_2   An object from class Button representing second floor button
        @param motor      An object from class MotorDriver representing a DC motor.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for the command to go to first floor
        self.button_1 = button_1
        
        ## The button object used for the command to go to second floor
        self.button_2 = button_2
        
        ## The button object used for the first floor sensor/limit
        self.first = first
        
        ## The button object used for the second floor sensor/limit
        self.second = second
        
        ## The motor object "moving" the elevator up and down
        self.motor = motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = interval
        
        ## The timestamp for the first iteration
        self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = self.start_time + self.interval
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.motor.Downward(2)
                print(str(self.runs) + ': State 0 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S3_STOPPED_ON_FLOOR_1):
                # Run State 3 Code
                
                if(self.runs*self.interval > 1):  # if our tsk runs for 10 seconds, transition
                    self.transitionTo(self.S5_DO_NOTHING)
                
                if(self.button_2.getButtonState(0)):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.button_2.getButtonState(1)
                    self.first.getButtonState(1)
                    self.motor.Upward(1)
                print(str(self.runs) + ': State 3 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                # Run State 4 Code
                if(self.button_1.getButtonState(0)):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.button_1.getButtonState(1)
                    self.second.getButtonState(1)
                    self.motor.Downward(2)
                print(str(self.runs) + ': State 4 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Transition to state 3 if the "first" switch is active
                if(self.first.getButtonState(0)):
                    self.transitionTo(self.S3_STOPPED_ON_FLOOR_1)
                    self.motor.Stop(0)
                print(str(self.runs) + ': State 1 ' + str(self.curr_time-self.start_time))
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                if(self.second.getButtonState(0)):
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    self.motor.Stop(0)
                print(str(self.runs) + ': State 2 ' + str(self.curr_time-self.start_time))
                
            elif(self.state == self.S5_DO_NOTHING):
                # Run state 5 code
                print(str(self.runs) + ': State 5 ' + str(self.curr_time-self.start_time))
            
            else:
                # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
             # Specifying the next time the task will run
            self.next_time = self.next_time + self.interval
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                imaginary driver to turn on or off the wipers. As of right now
                this class is implemented using "pseudo-hardware". That is, we
                are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))


    def getButtonState(self, val):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        
        if val == 0:
            return choice ([True, False])
        else:
            return False

        

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                move upward and downward.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Upward(self, v_up):
        '''
        @brief Moves the elevator upward
        '''
        print('Elevator moving up')
        if v_up == 1:
            return choice ([True])
            
        else:
            return False
    
    def Downward(self, v_down):
        '''
        @brief Moves the elevator downward
        '''
        print('Elevator moving down')
        if v_down == 2:
            return choice ([True])
            
        else:
            return False
    def Stop(self, v_stop):
        '''
        @brief Stops the elevator
        '''
        print('Elevator Stopped')
        if v_stop == 0:
            return choice ([True])
            
        else:
            return False
