##  @file BLE_Driver.py
#   Documentation for BLE_Driver.py, which contains code that defines the BLE_Drive class and all the methods required for our BLE_Task.py file to work properly. These methods include Write(), Read(),
#   Any(), ON(), and OFF(). This driver file should allow us to read from, write to, and check if any characters are waiting on the UART. It should also allow us to turn on and off the user LED. 
#
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab5/BLE_Driver.py
#
#   @author Keanau Robin
#
#



import pyb
from pyb import UART



class BLE_Drive:
    '''
    @brief      Encoder class with all the methods for running the Encoder task.
    @details    This class defines all the methods required for the user to properly run the Encoder. These methods are used in the Encoder FSM.
    '''
    def __init__(self,num):
        '''
        @brief Creates an Encoder Object
        '''
        self.num = num
        self.myuart = UART(3, 9600)
        
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        pass
        
    def Write(self):
        '''
        @brief  This function allows us to write to the UART
        '''
        self.myuart.write()
        
    def Read(self):
        '''
        @brief  This function allows us to read from the UART
        '''
        # self.myuart.readline()
        outp = self.myuart.readline()
        return outp
        
    def Any(self):
        '''
        @brief  This function allows us to check if any characters are waiting on the UART
        '''
        value = self.myuart.any()
        return value
        
    def ON(self):
        '''
        @brief  This function allows us to turn on the user LED
        '''
        self.pinA5.high()
        
    def OFF(self):
        '''
        @brief  This function allows us to turn off the user LED
        '''
        self.pinA5.low()
