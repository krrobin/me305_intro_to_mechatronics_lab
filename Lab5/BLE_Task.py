##  @file BLE_Task.py
#   Documentation for BLE_Task.py, which contains the BLE_Task UI task that waits for inputs from the phone via BTHC05connect app,and drives the LED appropriately with a frequency
#   choice between 1 to 10 Hz. When the user inputs their desired LED blinking frequency on the phone app, that frequency will be the input of this UI task. The UI task will take
#   the frequency input and use it as the new frequency of the LED on the Nucleo. 
#   
#   File can be found here: https://bitbucket.org/krrobin/me305_intro_to_mechatronics_lab/src/master/Lab5/BLE_Task.py
#
#   @author Keanau Robin
#



from BLE_Driver import BLE_Drive
import utime
import pyb
from pyb import UART

# import time

class BLE_Task:
    '''
    @brief     A finite state machine that updates the Nucleo's LED blinking frequency based on the user's desired frequency.
    @details   This class implements a finite state machine that goes through a state of waiting for the user's desired frequency. Once the desired frequency is received, it will be
               implemented onto the Nucleo's LED blinking frequency. The user will be capable of changing the frequency between an integer value of 1 to 10 Hz at any time.
    '''
    
    
    ## Costant defining State 0 - Initialization
    
    S0_INIT             = 0
    
    S1_WAIT_FOR_FRQ     = 1
    
    S2_LED_PATTERN      = 2
    
    def __init__(self, BLE_Drive):
        
        self.BLE_Drive = BLE_Drive
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##  The amount of time in seconds between runs of the task
        self.interval = 1e3
        
        self.FRQ = []
        
        self.counts = 0
        
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_ms() # The millisecond timestamp for the start-time of the task
        #self.start_time = time.time()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, int(self.interval))
        # self.next_time = self.start_time + self.interval
        
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_ms()
        # self.curr_time = time.time()
        if (utime.ticks_diff(self.curr_time, self.next_time)>=0):
        # if self.curr_time > self.next_time:

            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_FRQ)
            
            elif(self.state == self.S1_WAIT_FOR_FRQ):
                # Run State 1 Code
                # self.BLE_Driver.Read()
                if self.BLE_Drive.Any() != 0:
                    self.FRQ = int(self.BLE_Drive.Read())
                    # self.tmpFRQ = int(self.FRQ)
                    print(self.FRQ)
                    # print(self.tmpFRQ)
                    if self.FRQ > 0 and self.FRQ <= 10:
                            self.interval = 1e3/(2*self.FRQ)
                            self.BLE_Drive.Write('LED is blinking at ' + str(self.FRQ) + 'Hz')
                            self.transitionTo(self.S2_LED_PATTERN)
                    else:
                        print('Please select a frequency integer value between 1 to 10 Hz.')
                        pass
                else:
                    self.transitionTo(self.S1_WAIT_FOR_FRQ)
                    
            elif(self.state == self.S2_LED_PATTERN):
                # Run State 2 Code
                self.counts += self.interval/2
                print(self.interval)
                self.BLE_Drive.ON
                
                if self.counts == self.interval:
                    self.BLE_Drive.OFF
                    self.counts = 0
                    
                if self.BLE_Drive.Any() != 0:
                    self.transitionTo(self.S1_WAIT_FOR_FRQ)
                    self.counts = 0
                    self.interval = 0
                else:
                    self.transitionTo(self.S2_LED_PATTERN)
                
                
            else:
                    # Invalid state code (error handling)
                pass
            
            self.runs += 1
            
                # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, int(self.interval))
                # self.next_time = self.next_time + self.interval


    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
            
mydriver = BLE_Drive(0)

# Creating the task objects
task1 = BLE_Task(mydriver)

# Run the tasks in sequence over and over again
while True:
    task1.run()
