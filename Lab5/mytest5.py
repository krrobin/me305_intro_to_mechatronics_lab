
import pyb
from pyb import UART

myuart = UART(3, 9600)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

while True:
    if myuart.any() != 0:
        val = int(myuart.readline())
        if val == 0:
            print(val, 'Turns it OFF')
            pinA5.low()
        elif val == 1:
            print(val, 'Turns it ON')
            pinA5.high()
                